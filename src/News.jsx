import React from "react"
import axios from "axios"
import { Breadcrumb, Row, Col, Menu } from "antd"
import { BarsOutlined } from "@ant-design/icons"
import "./index.css"

class News extends React.Component {
  constructor(props) {
    super(props)
    this.state = { data: "" }
  }
  componentDidMount() {
    var th = this
    axios
      .get("http://39.107.31.29:5000/api/news")
      .then((res) => {
        // console.log(res.data)
        th.setState({ data: res.data })
      })
      .catch((e) => {
        console.log(e)
      })
  }
  componentWillUnmount() {
    this.setState = () => false
  }
  render() {
    const arr = []
    for (let i = 0; i < this.state.data.length; i++) {
      let v = (
        <div key={i} className='item'>
          <Row gutter={15}>
            <Col span={6}>
              <div className='img'>
                <img src={this.state.data[i].url} alt='tp' />
                <div className='img_cover'>{this.state.data[i].title}</div>
              </div>
            </Col>
            <Col span={18}>
              <div className='text'>
                <h2>
                  <a href='/#'>{this.state.data[i].title}</a>
                </h2>
                <p className='info'>
                  <span>服务对象及范围</span>
                  {this.state.data[i].type}
                </p>
                <p className='info'>
                  <span>服务时间</span>
                  {this.state.data[i].time}
                </p>
                <p>{this.state.data[i].dec}</p>
              </div>
            </Col>
          </Row>
        </div>
      )
      arr.push(v)
    }
    return (
      <div className='wrap content'>
        <div className='bread'>
          <Breadcrumb separator='>'>
            <Breadcrumb.Item>天气数据分析系统</Breadcrumb.Item>
            <Breadcrumb.Item>天气新闻页面</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <div className='wrap_con'>
          <Row>
            <Col span={6}>
              <div className='con_menu'>
                <Menu mode='vertical' selectedKeys={["index", "item"]} className='menu'>
                  <Menu.Item key='index'>
                    <BarsOutlined />
                    &nbsp;&nbsp;栏目列表
                  </Menu.Item>
                  <Menu.Item>气象科普</Menu.Item>
                  <Menu.Item>动态资讯</Menu.Item>
                  <Menu.Item key='item'>服务案例</Menu.Item>
                  <Menu.Item>标准规范</Menu.Item>
                  <Menu.Item>服务快报</Menu.Item>
                </Menu>
              </div>
            </Col>
            <Col span={18}>
              <div>{arr}</div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default News
