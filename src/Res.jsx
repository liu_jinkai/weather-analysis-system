import React from "react"
import logimg from "./img/login.png"
import { Form, Input, Button, Modal } from "antd"
import axios from "axios"

class Res extends React.Component {
  constructor(props) {
    super(props)
    this.state = { msg: "", show: false }
  }
  submitfun = async (values) => {
    this.setState({ msg: "" })
    let res = await axios.get("http://39.107.31.29:5000/api/toname", {
      params: {
        username: values.username,
        tel: values.tel,
        email: values.email,
      },
    })
    console.log(res)
    if (/^\w{5,20}$/gi.test(values.username) == false) {
      this.setState({ msg: "用户名只能包含字母，数字，下划线，长度为5-20" })
    } else if (/^\w{5,20}$/gi.test(values.password1) == false) {
      this.setState({ msg: "密码不能为空，长度为5-20" })
    } else if (values.password1 != values.password2) {
      this.setState({ msg: "两次密码输入不一致" })
    } else if (/^\w+@\w+\.com$/gi.test(values.email) == false) {
      this.setState({ msg: "邮箱格式不正确" })
    } else if (/^\d{11}$/.test(values.tel) == false) {
      this.setState({ msg: "手机格式不正确" })
    } else {
      let th = this
      this.setState({ msg: res.data.msg })
      if (res.data.type) {
        axios
          .post("http://39.107.31.29:5000/api/res", {
            data: values,
          })
          .then(function (data) {
            if (data.data.type) {
              th.setState({ msg: data.data.msg, show: true })
            }
          })
          .catch(function (e) {
            console.log(e)
          })
      }
    }
  }
  okfun = () => {
    this.setState({ show: false })
    this.props.toLogin()
  }
  cancelfun = () => {
    this.setState({ show: false })
  }
  render() {
    return (
      <div className='wrap content logindiv'>
        <Modal
          title='提示'
          cancelText='取消'
          okText='确认'
          visible={this.state.show}
          onOk={this.okfun}
          onCancel={this.cancelfun}
        >
          <p>注册成功！</p>
        </Modal>
        <div>
          <img src={logimg} alt='logimg' />
        </div>
        <div className='fom'>
          <h2>系统注册页面</h2>
          <h4>{this.state.msg}</h4>
          <Form labelCol={{ span: 5 }} wrapperCol={{ span: 18 }} onFinish={this.submitfun}>
            <Form.Item
              label='用户名'
              name='username'
              rules={[{ required: true, message: "请输入用户名" }]}
            >
              <Input></Input>
            </Form.Item>
            <Form.Item
              label='密码'
              name='password1'
              rules={[{ required: true, message: "请输入密码" }]}
            >
              <Input.Password></Input.Password>
            </Form.Item>
            <Form.Item
              label='确认密码'
              name='password2'
              rules={[{ required: true, message: "请输入密码" }]}
            >
              <Input.Password></Input.Password>
            </Form.Item>
            <Form.Item
              label='邮箱'
              name='email'
              rules={[{ required: true, message: "请输入邮箱" }]}
            >
              <Input></Input>
            </Form.Item>
            <Form.Item
              label='手机号'
              name='tel'
              rules={[{ required: true, message: "请输入手机号" }]}
            >
              <Input></Input>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 10, span: 20 }}>
              <Button type='primary' htmlType='submit'>
                注 册
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    )
  }
}

export default Res
