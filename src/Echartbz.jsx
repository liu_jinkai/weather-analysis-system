import React from "react"
import axios from "axios"
import { Select, Button, Breadcrumb } from "antd"
import * as echarts from "echarts"
import "./index.css"
import "./echart.css"

class Echartbz extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      provarr: [],
      cityarr: [],
      cityname: "",
      cname: "",
      str: "",
      year: "",
      data: {},
    }
  }
  componentDidMount() {
    let th = this
    axios
      .get("http://39.107.31.29:5000/api/province")
      .then((res) => {
        // console.log(res)
        th.setState({ provarr: res.data })
      })
      .catch((e) => {
        console.log(e)
      })
  }
  componentWillUnmount() {
    this.setState = () => false
  }
  selcity = (values) => {
    // console.log(values)
    let th = this
    axios
      .get("http://39.107.31.29:5000/api/city", {
        params: { id: values },
      })
      .then((res) => {
        // console.log(res)
        th.setState({ cityarr: res.data })
      })
      .catch((e) => {
        console.log(e)
      })
  }
  selcityname = (values, option) => {
    this.setState({
      cityname: values,
      cname: option.children,
    })
    // console.log(option)
  }
  yearfun = (values) => {
    this.setState({ year: values })
  }
  selechart = () => {
    let th = this
    this.setState({ str: "" })
    axios
      .get("http://39.107.31.29:5000/api/bzechart", {
        params: { cityname: th.state.cityname, year: th.state.year },
      })
      .then((res) => {
        // console.log(res)
        if (typeof res.data === "string") {
          th.setState({ str: res.data })
        } else {
          let option = {
            title: {
              text: th.state.year + th.state.cname + "空气质量分析",
              x: "center",
              y: "8%",
            },
            tooltip: { trigger: "item" },
            legend: { orient: "horizontal" },
            grid: {
              top: "16%",
              left: "3%",
              right: "8%",
              bottom: "3%",
              containLabel: true,
            },
            series: [{ type: "pie", name: "空气质量", radius: "65%", data: res.data }],
          }
          let myechart = echarts.init(document.getElementById("charts"))
          myechart.setOption(option)
        }
      })
      .catch((e) => {
        console.log(e)
      })
  }
  render() {
    const { Option } = Select
    let pros = this.state.provarr.map((value, index) => {
      return (
        <Option key={value.id} value={value.id}>
          {value.pname}
        </Option>
      )
    })
    let citys = this.state.cityarr.map((value, index) => {
      return (
        <Option key={value.id} value={value.cityname}>
          {value.cname}
        </Option>
      )
    })
    const charts = <div id='charts'></div>
    const str = <h2 id='echart_nomsg'>{this.state.str}</h2>
    return (
      <div className='wrap content'>
        <div className='bread'>
          <Breadcrumb separator='>'>
            <Breadcrumb.Item>天气数据分析系统</Breadcrumb.Item>
            <Breadcrumb.Item>饼状图分析</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <div className='selwrap'>
          <span className='sel_item'>省：&nbsp;</span>
          <Select onChange={this.selcity} className='select'>
            {pros}
          </Select>
          <span className='sel_item'>城市：&nbsp;</span>
          <Select onChange={this.selcityname} className='select'>
            {citys}
          </Select>
          <span className='sel_item'>年份：&nbsp;</span>
          <Select onChange={this.yearfun} className='select'>
            <Option value='2021'>2021</Option>
          </Select>
          <Button type='primary' onClick={this.selechart} className='sel_btn'>
            查询
          </Button>
        </div>
        <div>{this.state.str === "" ? charts : str}</div>
      </div>
    )
  }
}

export default Echartbz
