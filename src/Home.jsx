import "./index.css"
import React from "react"
import { Row, Col, Carousel, Button, Modal } from "antd"
import "./Home.css"
import login from "./img/user-head.png"
import { Link } from "react-router-dom"

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      bannarr: [
        new URL("./img/bann1.png", import.meta.url).href,
        new URL("./img/bann2.jpg", import.meta.url).href,
        new URL("./img/bann3.jpg", import.meta.url).href,
      ],
      titlearr: [
        { title: "地面气象资料", data: ["中国地面逐小时资料", "全球地面定时资料"] },
        { title: "高空气象资料", data: ["中国地面逐小时资料", "全球地面定时资料"] },
        { title: "卫星探测资料", data: ["大海区云图(IR1)", "扫描辐射计L1产品"] },
        { title: "天气雷达探测资料", data: ["基本反射率"] },
        { title: "数值预报模式产品", data: ["T639模式产品", "GRAPES模式产品"] },
      ],
      imgs: [
        new URL("./img/img1.png", import.meta.url).href,
        new URL("./img/img2.png", import.meta.url).href,
        new URL("./img/img3.png", import.meta.url).href,
      ],
      newsarr: [
        { title: "专业文献", url: new URL("./img/icon1.png", import.meta.url).href },
        { title: "院士专家", url: new URL("./img/icon2.png", import.meta.url).href },
        { title: "科研报告", url: new URL("./img/icon3.png", import.meta.url).href },
        { title: "资料清单", url: new URL("./img/icon4.png", import.meta.url).href },
        { title: "API服务", url: new URL("./img/icon5.png", import.meta.url).href },
        { title: "检索指定", url: new URL("./img/icon6.png", import.meta.url).href },
        { title: "图像展示", url: new URL("./img/icon7.png", import.meta.url).href },
        { title: "标准规范", url: new URL("./img/icon8.png", import.meta.url).href },
        { title: "专题数据", url: new URL("./img/icon9.png", import.meta.url).href },
      ],
      whetherarr: [
        {
          title: "3D台风专题",
          info: "数据采用国家气象信息中心研制的全球大气再分析数据产品CRA-40，以服务国家重大战略、气象防灾减灾为重点，践行“全球监测、全球预报、全球服务”理念， 为世界气象组织（WMO）成员国、“一带一路”沿线国家、丝绸之路经济核心区提供业务指导和数据服务。",
          icon: new URL("./img/clip.png", import.meta.url).href,
          url: new URL("./img/item1.png", import.meta.url).href,
        },
        {
          title: "青藏高原数据专题",
          info: "青藏高原气象业务观测、历次科学实验、长期野外观测的基本情况、观测内容、观测环境、观测仪器等。",
          icon: new URL("./img/clip.png", import.meta.url).href,
          url: new URL("./img/item2.png", import.meta.url).href,
        },
        {
          title: "气象环境专题",
          info: "本栏目提供全国范围的雾累计数据。包括雾年累计日数滚动数据和轻雾年累计日数滚动数据。",
          icon: new URL("./img/clip.png", import.meta.url).href,
          url: new URL("./img/item3.png", import.meta.url).href,
        },
        {
          title: "气象预警专题",
          info: " 本栏目主要基于WebGIS，发布国、省、市、县发布的大风、雷电、寒潮、干旱、冰雹等近20种自然灾害，同时，预警展示中结合了实况、预报数据，对预警相关气象要素的动态进行综合展示。",
          icon: new URL("./img/clip.png", import.meta.url).href,
          url: new URL("./img/item4.png", import.meta.url).href,
        },
      ],
      servearr: [
        {
          title: "“一带一路”专题服务",
          info: "气象平台通过开展“一带一路”专题服务，满足了沿线国家人民生活和社会发展方面的气象服务需求，加强并推动了沿线国家气象数据的共享与人才培养，提升了气象防灾减灾能力、在加强应对气候变化、服务生态文明等方面取得了显著的成效。",
          url: new URL("./img/case1.jpg", import.meta.url).href,
        },
        {
          title: "干旱知识服务",
          info: "针对收集的基础数据和产品、干旱各类观测资料和产品，特别是中国干旱气象科学试验研究观测资料的集成整合，使之符合 通用的国际标准，极大地提高其可用性和科学价值，实现数据增值；通过建立数据库和共享系统，使之被 更多的用户使用，极大地发挥其社会效益",
          url: new URL("./img/case2.png", import.meta.url).href,
        },
        {
          title: "新冠肺炎疫情专题服务",
          info: "本栏目基于WebGIS，展示新冠肺炎疫情的全国分布统计信息。结合全球的气温、风、降水、能见度、雷达、卫星等各类气象要素的实况、历史、预报数据同步展示。",
          url: new URL("./img/case3.png", import.meta.url).href,
        },
      ],
      show: false,
    }
  }
  extfun = () => {
    this.setState({ show: true })
  }
  okfun = () => {
    localStorage.clear()
    // localStorage.setItem("names", "")
    this.setState({ show: false })
  }
  cancelfun = () => {
    this.setState({ show: false })
  }
  render() {
    let titles = this.state.titlearr.map(function (value, index) {
      return (
        <li key={index}>
          <h3>{value.title}</h3>
          {value.data.map(function (v, i) {
            return <p key={i}>{v}</p>
          })}
        </li>
      )
    })
    const banns = this.state.bannarr.map(function (value, index) {
      return (
        <div key={index}>
          <img src={value} alt='ban' />
        </div>
      )
    })
    const imgs = this.state.imgs.map(function (value, index) {
      return (
        <div key={index}>
          <img src={value} alt='img' />
        </div>
      )
    })
    const newsimg = this.state.newsarr.map(function (value, index) {
      return (
        <li key={index}>
          <img src={value.url} alt='tp' />
          <p>{value.title}</p>
        </li>
      )
    })
    const whether = this.state.whetherarr.map(function (value, index) {
      return (
        <Col span={6} key={index}>
          <div className='wheatheritem'>
            <div className='wheatherimg'>
              <img src={value.url} alt='item' />
            </div>
            <div className='wheathertext'>
              <img src={value.icon} alt='icon' />
              <h3>{value.title}</h3>
              <p>{value.info}</p>
            </div>
            <div className='wheatherhover'>
              <img src={value.url} alt='img' />
              <div>
                <img src={value.icon} alt='icon' />
                <h3>{value.title}</h3>
                <p>{value.info}</p>
              </div>
            </div>
          </div>
          <div className=''></div>
        </Col>
      )
    })
    const serves = this.state.servearr.map(function (value, index) {
      return (
        <Col span={8} key={index}>
          <div className='serveitem'>
            <div className='servehover'>
              <img src={value.url} alt='serve' />
              <div className='servemsg'>
                <p>{value.info}</p>
                <a href='/#'>查看详情</a>
              </div>
            </div>
            <h2>{value.title}</h2>
          </div>
        </Col>
      )
    })
    // 登录判断
    let pdlogin = ""
    if (localStorage.getItem("names") == "" || localStorage.getItem("names") == null) {
      pdlogin = (
        <div className='clearbox'>
          <img src={login} alt='lg' className='fl' />
          <p>欢迎登录天气数据分析系统</p>
        </div>
      )
    } else {
      pdlogin = (
        <div className='clearbox'>
          <img src={login} alt='lg' className='fl' />
          <p>
            欢迎[<a href='/#'>{localStorage.getItem("names")}</a>]登录，
            <a href='/#' onClick={this.extfun}>
              退出系统
            </a>
          </p>
        </div>
      )
    }
    return (
      <div className='wrap'>
        <Modal
          title='提示'
          cancelText='取消'
          okText='确认'
          visible={this.state.show}
          onOk={this.okfun}
          onCancel={this.cancelfun}
        >
          <p>你确定要退出吗？</p>
        </Modal>
        <div className='info'>
          <Row gutter={15}>
            <Col span={4} className='box'>
              <div className='titles'>
                <h3>数据分析体系</h3>
                <ul>{titles}</ul>
              </div>
            </Col>
            <Col span={11} className='box'>
              <div>
                <Carousel autoplay>{banns}</Carousel>
              </div>
            </Col>
            <Col span={4} className='box'>
              <div className='imgs'>{imgs}</div>
            </Col>
            <Col span={5} className='box'>
              <div>
                <div className='login'>
                  {pdlogin}
                  <div>
                    <Link to='/login'>
                      <Button type='primary' shape='round'>
                        登 录
                      </Button>
                    </Link>
                    <Link to='/res'>
                      <Button type='primary' shape='round'>
                        注 册
                      </Button>
                    </Link>
                  </div>
                </div>
                <div className='news'>
                  <div className='newstitle clearbox'>
                    <h4 className='fl'>服务快报</h4>
                    <a href='/#' className='fr'>
                      更多
                    </a>
                  </div>
                  <ul>
                    <li>
                      <span>最新</span>
                      <a href='/#'>全球新冠肺炎气象服务快报</a>
                    </li>
                    <li>
                      <span>最新</span>
                      <a href='/#'>全球新冠肺炎气象服务快报</a>
                    </li>
                    <li>
                      <span>最新</span>
                      <a href='/#'>全球新冠肺炎气象服务快报</a>
                    </li>
                    <li>
                      <span>最新</span>
                      <a href='/#'>全球新冠肺炎气象服务快报</a>
                    </li>
                    <li>
                      <span>最新</span>
                      <a href='/#'>全球新冠肺炎气象服务快报</a>
                    </li>
                  </ul>
                </div>
                <div className='newsicon'>
                  <ul>{newsimg}</ul>
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <div className='wheather'>
          <h2>气象专题</h2>
          <div>
            <Row gutter={20}>{whether}</Row>
          </div>
        </div>
        <div className='serve'>
          <h2>服务案例</h2>
          <div>
            <Row gutter={20}>{serves}</Row>
          </div>
        </div>
      </div>
    )
  }
}

export default Home
