import React from "react"
import logimg from "./img/login.png"
import { Form, Input, Button, Modal } from "antd"
import axios from "axios"

class Passr extends React.Component {
  constructor(props) {
    super(props)
    this.state = { msg: "", show: false }
  }
  submitfun = (values) => {
    this.setState({ msg: "" })
    if (/^\w+@\w+\.com$/.test(values.email) == false) {
      this.setState({ msg: "邮箱格式不正确" })
    } else if (/^\w{5,20}$/.test(values.password1) == false) {
      this.setState({ msg: "密码为数字，字母，下划线，长度在5-20位" })
    } else if (values.password1 != values.password2) {
      this.setState({ msg: "两次密码输入不一致" })
    } else {
      let th = this
      axios
        .post("http://39.107.31.29:5000/api/passr", {
          data: values,
        })
        .then(function (res) {
          if (res.data.type == false) {
            th.setState({ msg: res.data.msg })
          } else {
            th.setState({ show: true })
          }
        })
        .catch(function (e) {
          console.log(e)
        })
    }
  }
  okfun = () => {
    this.setState({ show: false })
    this.props.toLogin()
  }
  cancelfun = () => {
    this.setState({ show: false })
  }
  render() {
    return (
      <div className='wrap content logindiv'>
        <Modal
          title='提示'
          cancelText='取消'
          okText='确认'
          visible={this.state.show}
          onOk={this.okfun}
          onCancel={this.cancelfun}
        >
          <p>修改成功！</p>
        </Modal>
        <div>
          <img src={logimg} alt='logimg' />
        </div>
        <div className='fom'>
          <h2>设置密码</h2>
          <h4>{this.state.msg}</h4>
          <Form labelCol={{ span: 5 }} wrapperCol={{ span: 18 }} onFinish={this.submitfun}>
            <Form.Item
              label='邮箱'
              name='email'
              rules={[{ required: true, message: "请输入邮箱" }]}
            >
              <Input></Input>
            </Form.Item>
            <Form.Item
              label='密码'
              name='password1'
              rules={[{ required: true, message: "请输入密码" }]}
            >
              <Input.Password></Input.Password>
            </Form.Item>
            <Form.Item
              label='确认密码'
              name='password2'
              rules={[{ required: true, message: "请输入密码" }]}
            >
              <Input.Password></Input.Password>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 10, span: 20 }}>
              <Button type='primary' htmlType='submit'>
                提 交
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    )
  }
}

export default Passr
