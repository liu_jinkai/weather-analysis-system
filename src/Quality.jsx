import React from "react"
import axios from "axios"
import { Select, Button, Breadcrumb, Pagination, Table } from "antd"
import "./index.css"
import "./weather.css"
import "./quality.css"

const { Column, ColumnGroup } = Table
class Quality extends React.Component {
  constructor(props) {
    super(props)
    this.tabs = React.createRef()
    this.state = {
      provarr: [],
      cityarr: [],
      cityname: "",
      cname: "",
      str: "",
      year: "",
      kqzlarr: [],
      page: 1,
      count: 10,
      scount: 0,
    }
  }
  componentDidMount() {
    let th = this
    axios
      .get("http://39.107.31.29:5000/api/province")
      .then((res) => {
        // console.log(res)
        th.setState({ provarr: res.data })
      })
      .catch((e) => {
        console.log(e)
      })
  }
  componentWillUnmount() {
    this.setState = () => false
  }
  selcity = (values) => {
    // console.log(values)
    let th = this
    axios
      .get("http://39.107.31.29:5000/api/city", {
        params: { id: values },
      })
      .then((res) => {
        // console.log(res)
        th.setState({ cityarr: res.data })
      })
      .catch((e) => {
        console.log(e)
      })
  }
  selcityname = (values, option) => {
    this.setState({
      cityname: values,
      cname: option.children,
    })
    // console.log(option)
  }
  selkqzl = () => {
    this.ajaxfun(1)
  }
  yearfun = (values) => {
    this.setState({ year: values })
  }
  pagefun = (values) => {
    // this.setState({ page: values })
    this.ajaxfun(values)
  }
  ajaxfun(num) {
    let th = this
    axios
      .post("http://39.107.31.29:5000/api/kqzl", {
        data: {
          cityname: th.state.cityname,
          year: th.state.year,
          page: num,
          count: th.state.count,
        },
      })
      .then((res) => {
        console.log(res)
        th.setState({ str: "", kqzlarr: [] })
        if (typeof res.data == "string") {
          th.setState({ str: res.data })
        } else {
          th.setState({
            kqzlarr: res.data.data,
            page: res.data.page,
            count: res.data.count,
            scount: res.data.scount,
          })
        }
      })
      .catch((e) => {
        console.log(e)
      })
  }
  render() {
    const { Option } = Select
    let pros = this.state.provarr.map((value, index) => {
      return (
        <Option key={value.id} value={value.id}>
          {value.pname}
        </Option>
      )
    })
    let citys = this.state.cityarr.map((value, index) => {
      return (
        <Option key={value.id} value={value.cityname}>
          {value.cname}
        </Option>
      )
    })
    let str = <h2>{this.state.str}</h2>
    let tab = (
      <Table dataSource={this.state.kqzlarr}>
        <Column title='日期' dataIndex='date' key='date' />
        <Column title='质量等级' dataIndex='kqzl' key='kqzl' />
        <Column title='AQI指数' dataIndex='aqi' key='aqi' />
        <Column title='PM2.5' dataIndex='pm25' key='pm25' />
        <Column title='PM10' dataIndex='pm10' key='pm10' />
        <Column title='SO2' dataIndex='so2' key='so2' />
        <Column title='NO2' dataIndex='no2' key='no2' />
        <Column title='CO' dataIndex='co' key='co' />
        <Column title='O3' dataIndex='o3' key='o3' />
      </Table>
    )
    return (
      <div className='wrap content tqybwrap qualitywrap'>
        <div className='bread'>
          <Breadcrumb separator='>'>
            <Breadcrumb.Item>天气数据分析系统</Breadcrumb.Item>
            <Breadcrumb.Item>空气质量</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <div className='selwrap'>
          <span className='sel_item'>省：&nbsp;</span>
          <Select onChange={this.selcity} className='select'>
            {pros}
          </Select>
          <span className='sel_item'>城市：&nbsp;</span>
          <Select onChange={this.selcityname} className='select'>
            {citys}
          </Select>
          <span className='sel_item'>年份：&nbsp;</span>
          <Select onChange={this.yearfun} className='select'>
            <Option value='2021'>2021</Option>
          </Select>
          <Button type='primary' onClick={this.selkqzl} className='sel_btn'>
            查询
          </Button>
        </div>
        <h1>
          {this.state.year}
          {this.state.cname}空气质量指数数据
        </h1>
        <div ref={this.tabs} className='kqzl_tab'>
          {tab}
          {/* {this.state.str === "" ? tab : str} */}
        </div>
        <div className={`page ${this.state.str === "" ? "show" : "hidden"}`}>
          <Pagination
            total={this.state.scount}
            pageSize={this.state.count}
            current={this.state.page}
            onChange={this.pagefun}
            hideOnSinglePage
          />
        </div>
      </div>
    )
  }
}

export default Quality
