import React from "react"
import logimg from "./img/login.png"
import { Form, Input, Button } from "antd"
import { Link } from "react-router-dom"
import axios from "axios"

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = { msg: "" }
  }
  submitfun(values) {
    let th = this
    axios
      .post("http://39.107.31.29:5000/api/login", {
        data: values,
      })
      .then(function (res) {
        if (res.data.type == false) {
          th.setState({ msg: res.data.msg })
        } else {
          th.props.toIndex(res.data.msg)
        }
      })
      .catch(function (e) {
        console.log(e)
      })
  }
  render() {
    return (
      <div className='wrap content logindiv'>
        <div>
          <img src={logimg} alt='logimg' />
        </div>
        <div className='fom'>
          <h2>系统登录页面</h2>
          <h4>{this.state.msg}</h4>
          <Form
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 18 }}
            onFinish={this.submitfun.bind(this)}
          >
            <Form.Item
              label='用户名'
              name='username'
              rules={[{ required: true, message: "请输入用户名" }]}
            >
              <Input></Input>
            </Form.Item>
            <Form.Item
              label='密码'
              name='password'
              rules={[{ required: true, message: "请输入密码" }]}
            >
              <Input.Password></Input.Password>
            </Form.Item>
            <div className='clearbox'>
              <Link to='/passr' className='fl'>
                忘记密码
              </Link>
              <Link to='/res' className='fr'>
                立即注册
              </Link>
            </div>
            <Form.Item wrapperCol={{ offset: 10, span: 20 }}>
              <Button type='primary' htmlType='submit'>
                登 录
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    )
  }
}

export default Login
