import "./App.css"
import "./index.css"
import React from "react"

class Footer extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div className='footer'>
        <p>XX公司 天气版权所有 copyright @XX气象分析服务中国</p>
      </div>
    )
  }
}

export default Footer
